﻿using System.Net;
using System.Threading.Tasks;
using Android.Graphics;
using Showcase.Xamarin.BL.Contracts.Services;

namespace Showcase.Xamarin.Droid.Resources
{
    public class ResourceService : IResourcesService
    {
        public async Task<T> GetImageFromUrl<T>(string url) where T : class
        {
            using (var webClient = new WebClient())
            {
                var imageBytes = await webClient.DownloadDataTaskAsync(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    var imageBitmap = await BitmapFactory.DecodeByteArrayAsync(imageBytes, 0, imageBytes.Length);
                    return imageBitmap as T;
                }
            }

            return default(T);
        }
    }
}