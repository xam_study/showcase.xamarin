﻿using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V7.Widget;
using Android.Widget;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.ViewModels.Recipe;
using Showcase.Xamarin.Views;
using Showcase.Xamarin.ViewModels.Contracts.Recipe;

namespace Showcase.Xamarin.Droid.UI.Views
{
    public class RecipeItemView : RecyclerView.ViewHolder, IRecipeView
    {
        private readonly IResourcesService _resourceService;
        private readonly Android.Views.View _view;
        //todo: refactor
        private string _imageUrlStore;
        public IViewContext<RecipeViewModel> ViewContext { get; }


        public RecipeItemView(Android.Views.View itemView, IResourcesService resourceService) : base(itemView)
        {
            this._view = itemView;
            this._resourceService = resourceService;
            this.ViewContext = ViewContext<RecipeViewModel>.CreateViewContext(this);
        }

        #region Controls
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }

        #endregion

        public void Initialize()
        {
            this.Image = this._view.FindViewById<ImageView>(Resource.Id.imageView);
            this.Caption = this._view.FindViewById<TextView>(Resource.Id.textView);

            this.Image.Click += (sender, args) =>
            {
                this.ViewContext.ViewModel.OnDetailsClick
                (
                    this._view.Context,
                    EventArgsWithT<IRecipeViewModel, IRecipeViewModel>.Create
                    (
                        this.ViewContext.ViewModel, this.ViewContext.ViewModel
                    )
                );
            };
        }

        public async void Refresh()
        {
            this.Caption.Text = this.ViewContext.ViewModel.Description;

            //try to update only diff pictures
            if (this.ViewContext.ViewModel.ImageUrl != this._imageUrlStore)
            {
                this._imageUrlStore = this.ViewContext.ViewModel.ImageUrl;
                var bitmap = await this._resourceService.GetImageFromUrl<Bitmap>(this._imageUrlStore);
                ((BitmapDrawable) Image.Drawable)?.Bitmap?.Recycle();
                this.Image.SetImageBitmap(bitmap);
            }
        }
    }
}