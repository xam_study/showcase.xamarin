﻿using System.Linq;
using Android.App;
using Showcase.Xamarin.BL.Contracts.Controllers;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Droid.UI.Extensions;
using Showcase.Xamarin.Droid.UI.Views.Activities.Common;
using Showcase.Xamarin.ViewModels.Recipe;
using Showcase.Xamarin.Views;
using Showcase.Xamarin.Views.Bindings.Base;
using Showcase.Xamarin.Views.Bindings.Contracts;

namespace Showcase.Xamarin.Droid.UI.Views.Activities
{
    [Activity(Label = "Description")]
    public class DescriptionActivity : InitializedActivity
    {
        private IBindingBuilder<RecipeDetailsViewModel, IRecipeDetailsView> _bindingBuilder;
        private IRecipeController _recipeController;

        protected override int ContentViewId => Resource.Layout.Description;

        protected override void OnIocResolve(IIocService iocService)
        {
            base.OnIocResolve(iocService);

            this._recipeController = iocService.Resolve<IRecipeController>();
            this._bindingBuilder = new BindingBuilder<RecipeDetailsViewModel, IRecipeDetailsView>();
        }

        protected override void OnViewSubscriptionInitialize()
        {
            this._bindingBuilder.EventSubscription(model => model.OnGenerateNewDescription += (sender, args) =>
            {
                var newDescription = this._recipeController.GenerateNewRandomDescription();

                this._recipeController.UpdateRecipe
                (
                    //todo:
                    newDescription,
                    int.Parse(this.Intent.GetStringArrayListExtra(nameof(RecipeDetailsViewModel.Description)).Last())
                );

                var fragment = this.GetFragment<IRecipeDetailsView>();
                var viewModel = new RecipeDetailsViewModel()
                {
                    Description = newDescription
                };
                this._bindingBuilder.Bind(viewModel, fragment);
            });
        }

        protected override void OnViewInitialize()
        {
            this.InitializeFragment<IRecipeDetailsView>(Resource.Id.innerLayout);
        }

        protected override void OnViewDataInitialize()
        {
            var viewModel = new RecipeDetailsViewModel()
            {
                Description = this.Intent.GetStringArrayListExtra(nameof(RecipeDetailsViewModel.Description)).First()
            };
            this._bindingBuilder.Bind(viewModel, this.GetFragment<IRecipeDetailsView>());
        }
    }
}