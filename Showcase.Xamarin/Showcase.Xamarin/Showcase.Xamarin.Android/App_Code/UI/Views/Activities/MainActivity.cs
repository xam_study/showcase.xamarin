﻿using Android.App;
using Android.Support.V7.Widget;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Droid.UI.Contracts.Bindings;
using Showcase.Xamarin.Droid.UI.Extensions;
using Showcase.Xamarin.Droid.UI.Views.Activities.Common;

namespace Showcase.Xamarin.Droid.UI.Views.Activities
{
    [Activity(Label = "Showcase", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : InitializedActivity
    {
        private IRecipeViewAdapter _recipeViewAdapter;

        private RecyclerView.LayoutManager _layoutManager;
        private RecyclerView _recyclerView;

        protected override void OnResume()
        {
            base.OnResume();
            _recipeViewAdapter?.OnViewResume();
        }

        protected override void OnIocResolve(IIocService iocService)
        {
            this._recipeViewAdapter = iocService.Resolve<IRecipeViewAdapter>();
        }

        protected override int ContentViewId => Resource.Layout.Main;

        protected override void OnViewInitialize()
        {
            this._recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            this._layoutManager = new LinearLayoutManager(this);
            this._recyclerView.SetLayoutManager(this._layoutManager);
            this._recyclerView.SetAdapter(this._recipeViewAdapter);
        }
    }
}


