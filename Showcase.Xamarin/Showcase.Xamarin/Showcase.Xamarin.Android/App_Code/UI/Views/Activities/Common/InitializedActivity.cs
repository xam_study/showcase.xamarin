﻿using System.Linq;
using System.Reflection;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Extensions;

namespace Showcase.Xamarin.Droid.UI.Views.Activities.Common
{
    public abstract class InitializedActivity : Activity
    {
        [Inject(StartPoint = true)]
        public IIocService IocService { get; set; }

        protected InitializedActivity() : base()
        {
            this.InitializeObjectByIocContainerAndAddingGlobalHandlers
            (
                () =>
                {
                    //adding global exception handler
                    AndroidEnvironment.UnhandledExceptionRaiser += (sender, args) =>
                    {
                        Toast.MakeText(this, args.Exception.Message, ToastLength.Long).Show();
                    };
                },
                type => type.GetCustomAttributes<ActivityAttribute>(true).Any(c => c.MainLauncher)
            );
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.SetContentView(this.ContentViewId);
            this.OnIocResolve(this.IocService);
            this.OnViewSubscriptionInitialize();
            this.OnViewInitialize();
        }

        //todo: think about the lifecycle: https://developer.android.com/reference/android/app/Activity.html
        protected override void OnStart()
        {
            base.OnStart();
            this.OnViewDataInitialize();
        }

        protected virtual void OnViewInitialize()
        {
        }

        protected virtual void OnIocResolve(IIocService iocService)
        {
        }

        protected virtual void OnViewDataInitialize()
        {
        }

        protected virtual void OnViewSubscriptionInitialize()
        {
        }

        protected abstract int ContentViewId { get; }
    }
}