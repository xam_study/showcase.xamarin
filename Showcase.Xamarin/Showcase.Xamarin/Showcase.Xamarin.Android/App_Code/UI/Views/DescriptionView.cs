﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.ViewModels.Contracts.Recipe;
using Showcase.Xamarin.ViewModels.Recipe;
using Showcase.Xamarin.Views;

namespace Showcase.Xamarin.Droid.UI.Views
{
    public class DescriptionView : Fragment, IRecipeDetailsView
    {
        public IViewContext<RecipeDetailsViewModel> ViewContext { get; }

        private Button GenerateNewDescription { get; set; }
        private TextView Description { get; set; }

        public DescriptionView()
        {
            this.ViewContext = ViewContext<RecipeDetailsViewModel>.CreateViewContext(this);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            
            //think about calling this method automatically 
            this.Initialize();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.DescriptionFragment, container, false);
        }

        public void Refresh()
        {
            if (this.Activity != null)
            {
                this.Description.Text = this.ViewContext.ViewModel.Description;
            }
        }

        public void Initialize()
        {
            #region Generate button
            this.GenerateNewDescription = this.Activity.FindViewById<Button>(Resource.Id.randButton);
            this.GenerateNewDescription.Click += (sender, args) =>
            {
                this.ViewContext.ViewModel.OnGenerateNewDescription(this.Activity, EventArgsWithT<IRecipeDetailsViewModel>.Create(this.ViewContext.ViewModel));
            };
            #endregion
            this.Description = this.Activity.FindViewById<TextView>(Resource.Id.descriptionLabel);
        }
    }
}