﻿using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Showcase.Xamarin.BL.Contracts.Controllers;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.Droid.UI.Contracts.Bindings;
using Showcase.Xamarin.Droid.UI.Views.Activities;
using Showcase.Xamarin.Extensions;
using Showcase.Xamarin.ViewModels.Recipe;
using Showcase.Xamarin.Views;
using Showcase.Xamarin.Views.Bindings.Contracts;

namespace Showcase.Xamarin.Droid.UI.Views.Bindings
{
    public class RecipeViewAdapter : RecyclerView.Adapter, IRecipeViewAdapter, IInitialize
    {
        private readonly IRecipeController _recipeController;
        private readonly IIocService _iocService;
        private readonly IListViewModelsBindingBuilder<RecipeAlbumViewModel, RecipeViewModel, IRecipeView> _splittedBindingBuilder;

        public RecipeViewAdapter
        (
            IRecipeController recipeController,
            IIocService iocService,
            IListViewModelsBindingBuilder<RecipeAlbumViewModel, RecipeViewModel, IRecipeView> splittedBindingBuilder
        )
        {
            this._recipeController = recipeController;
            this._iocService = iocService;
            this._splittedBindingBuilder = splittedBindingBuilder;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var recipeView = holder as IRecipeView;
            this._splittedBindingBuilder.Bind(position, recipeView);
        }

        public override void OnAttachedToRecyclerView(RecyclerView recyclerView)
        {
            base.OnAttachedToRecyclerView(recyclerView);

            recyclerView.AddOnScrollListener(new ScrollListener(this._recipeController, this._splittedBindingBuilder));
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            Android.Views.View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.RecipeView, parent, false);

            var viewHolder = this._iocService.ResolveAndCheckInnerParams<IRecipeView, Android.Views.View, RecipeViewModel>(itemView);
            return viewHolder as RecyclerView.ViewHolder;
        }

        public override int ItemCount => this._splittedBindingBuilder.Count;

        public void Initialize()
        {
            //this._recipeController.RemoveAll();
            this._splittedBindingBuilder.Bind(this._recipeController.GetRecipeAlbumViewModel());
            this._splittedBindingBuilder.EventSubscription(viewModel =>
            {
                viewModel.OnDetailsClick += (sender, args) =>
                {
                    var context = (Context) sender;
                    Intent descriptionActivity = new Intent(context, typeof(DescriptionActivity));
                    descriptionActivity.PutStringArrayListExtra(nameof(args.NewValue.Description), new List<string>()
                    {
                        args.NewValue.Description,
                        //todo:
                        (args.NewValue as IId).Id.ToString()
                    });
                    
                    context.StartActivity(descriptionActivity);
                };
            });
        }

        public void OnViewResume()
        {
            this._splittedBindingBuilder.Bind(this._recipeController.GetRecipeAlbumViewModel());
            this.NotifyDataSetChanged();
        }
    }

    public class ScrollListener : RecyclerView.OnScrollListener
    {
        private readonly IRecipeController _recipeController;
        private readonly IListViewModelsBindingBuilder<RecipeAlbumViewModel, RecipeViewModel, IRecipeView> _splittedBindingBuilder;

        public ScrollListener(IRecipeController recipeController, IListViewModelsBindingBuilder<RecipeAlbumViewModel, RecipeViewModel, IRecipeView> splittedBindingBuilder)
        {
            this._recipeController = recipeController;
            this._splittedBindingBuilder = splittedBindingBuilder;
        }

        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            base.OnScrolled(recyclerView, dx, dy);
            var linerManager = (LinearLayoutManager)recyclerView.GetLayoutManager();
            var lastVisiblePosition = linerManager.FindLastVisibleItemPosition();
            var adapter = recyclerView.GetAdapter();

            if ((linerManager.ItemCount - (lastVisiblePosition + 1)) == 0 && dy > 0 /*to down*/)
            {
                this._recipeController.AddRecipe(this._splittedBindingBuilder.Collection[lastVisiblePosition]);
                this._splittedBindingBuilder.Bind(this._recipeController.GetRecipeAlbumViewModel());

                adapter.NotifyItemInserted(this._splittedBindingBuilder.Collection.Count - 1);
            }
        }
    }
}