﻿using Android.Support.V7.Widget;
using Showcase.Xamarin.Droid.UI.Contracts.Bindings;

namespace Showcase.Xamarin.Droid.UI.Extensions
{
    public static class RecyclerViewExtensions
    {
        public static void SetAdapter(this RecyclerView recylcerView, IViewAdapter viewAdapter)
        {
            if (viewAdapter is RecyclerView.Adapter adapter)
            {
                recylcerView.SetAdapter(adapter);
            }
            else
            {
                throw new System.Exception("The interface IViewAdapter should be used only with RecyclerView.Adapter");
            }
        }

        public static void NotifyDataSetChanged(this IViewAdapter viewAdapter)
        {
            if (viewAdapter is RecyclerView.Adapter adapter)
            {
                adapter.NotifyDataSetChanged();
            }
            else
            {
                throw new System.Exception("The interface IViewAdapter should be used only with RecyclerView.Adapter");
            }
        }
    }
}