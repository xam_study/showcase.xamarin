﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Showcase.Xamarin.Droid.UI.Views.Activities.Common;
using Showcase.Xamarin.Extensions;
using Showcase.Xamarin.ViewModels.Contracts;
using Showcase.Xamarin.Views;
using Uri = Android.Net.Uri;

namespace Showcase.Xamarin.Droid.UI.Extensions
{
    public static class NativeExtensions
    {
        public static void SetImageByUrl(this Android.Widget.ImageView image, string url)
        {
            image.SetImageURI(Uri.Parse(url));
        }

        public static void PutObject<TType>(this Bundle bundle, TType @object, string key) where TType : class
        {
            if (@object != null)
            {
                bundle.PutString(typeof(TType).FullName + (!string.IsNullOrWhiteSpace(key) ? key : string.Empty), @object.Serialize());
            }
        }

        public static TType GetObject<TType>(this Bundle bundle, string key) where TType : class
        {
            var str = bundle.GetString(typeof(TType).FullName + (!string.IsNullOrWhiteSpace(key) ? key : string.Empty));
            return str.Deserialize<TType>();
        }

        public static void PutObject<TType>(this Intent intent, TType @object, string key) where TType : class
        {
            if (@object != null)
            {
                Bundle bundle = new Bundle();
                var name = typeof(TType).FullName + (!string.IsNullOrWhiteSpace(key) ? key : string.Empty);
                bundle.PutString(name, @object.Serialize());
                intent.PutExtra(name, bundle);
            }
        }

        public static TType GetObject<TType>(this Intent intent, string key) where TType : class
        {
            var name = typeof(TType).FullName + (!string.IsNullOrWhiteSpace(key) ? key : string.Empty);
            var bundle = intent.GetBundleExtra(name);
            return bundle?.GetString(name).Deserialize<TType>();
        }

        public static TView GetFragment<TView>(this InitializedActivity activity) 
            where TView : class, IView<IViewModel>
        {
            var fragment = activity.FragmentManager.FindFragmentByTag(nameof(TView));
            if (fragment == null)
            {
                return activity.IocService.Resolve<TView>();
            }
            else
            {
                return fragment as TView;
            }
        }

        public static TView InitializeFragment<TView>(this InitializedActivity activity, int layoutId) 
            where TView : class, IView<IViewModel>
        {
            var linerLayout = activity.FindViewById<LinearLayout>(layoutId);
            if (linerLayout == null)
            {
                throw new ArgumentException("The layout was not found");
            }

            var fragment = activity.IocService.Resolve<TView>();
            var castedFragment = (fragment as Fragment);
            if (castedFragment == null)
            {
                throw new ArgumentException($"The fragment '{nameof(TView)}' was not found");
            }

            activity.DoUnderTransaction(transaction =>
            {
                transaction.Add(linerLayout.Id, castedFragment, nameof(TView));
            });

            return fragment;
        }

        public static void DoUnderTransaction(this Activity activity, Action<FragmentTransaction> action)
        {
            FragmentTransaction transaction = null;
            try
            {
                transaction = activity.FragmentManager.BeginTransaction();
                action?.Invoke(transaction);
            }
            finally
            {
                transaction?.Commit();
            }
        }
    }
}