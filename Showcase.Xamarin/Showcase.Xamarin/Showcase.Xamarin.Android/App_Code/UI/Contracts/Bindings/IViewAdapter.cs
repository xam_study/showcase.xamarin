﻿namespace Showcase.Xamarin.Droid.UI.Contracts.Bindings
{
    public interface IViewAdapter
    {
        void OnViewResume();
    }
}