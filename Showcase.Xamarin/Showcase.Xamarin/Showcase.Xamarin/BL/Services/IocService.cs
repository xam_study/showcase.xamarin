﻿using Showcase.Xamarin.BL.Contracts.Services;
using Autofac;
using Showcase.Xamarin.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Showcase.Xamarin.BL.Services
{
    public class IocService : IIocService
    {
        private readonly IComponentContext _componentContext;

        public IocService(IComponentContext componentContext)
        {
            this._componentContext = componentContext;
        }

        public TType Resolve<TType>() where TType : class
        {
            return this._componentContext.SafeResolve<TType>();
        }

        public TType ResolveWithParamByName<TType, TParamType>(string paramName, TParamType paramValue) where TType : class
        {
            return this.ResolveWithParamByName<TType, TParamType>(new [] { new KeyValuePair<string, TParamType>(paramName, paramValue) });
        }

        public TType ResolveWithParamByName<TType, TParamType>(IEnumerable<KeyValuePair<string, TParamType>> @params) where TType : class
        {
            return this._componentContext.Resolve<TType>(@params.Select(c => new NamedParameter(c.Key, c.Value)));
        }

        public TType ResolveWithParamByType<TType, TParamType>(TParamType paramValue) where TType : class
        {
            return this.ResolveWithParamByType<TType, TParamType>(new[] { paramValue });
        }

        public TType ResolveWithParamByType<TType, TParamType>(IEnumerable<TParamType> @params) where TType : class
        {
            return this._componentContext.Resolve<TType>(@params.Select(c => new TypedParameter(typeof(TParamType), c)));
        }
    }
}
