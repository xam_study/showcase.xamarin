﻿using System.Collections.Generic;
using System.Linq;
using Showcase.Xamarin.BL.Contracts.Services;
using System.Xml.Linq;

namespace Showcase.Xamarin.BL.Services
{
    public class ExternalContentService : IExternalContentService
    {
        public KeyValuePair<string, string>[] GetRssContent(string url)
        {
            //mock
            var document = XDocument.Load(url);
            var items = document.Descendants(XName.Get("item"));
            return items.Select(c =>
                new KeyValuePair<string, string>
                (
                    c.Descendants(XName.Get("guid")).First().Value,
                    c.Descendants(XName.Get("description")).First().Value
                )).ToArray();
        }
    }
}
