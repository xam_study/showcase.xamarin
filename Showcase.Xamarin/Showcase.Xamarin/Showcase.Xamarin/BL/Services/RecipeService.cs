﻿using System.Collections.Generic;
using Showcase.Xamarin.BL.Contracts.Repositories;
using Showcase.Xamarin.BL.Contracts.Repositories.Base;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Models.Recipe;

namespace Showcase.Xamarin.BL.Services
{
    public class RecipeService : IRecipeService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RecipeService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            this._unitOfWorkFactory = unitOfWorkFactory;
        }

        public void RemoveAll()
        {
            using (var unitOfWork = this._unitOfWorkFactory.Connect())
            {
                unitOfWork.Get<IRecipeRepository>().RemoveAll();
            }
        }

        public List<Recipe> GetRecipesBy(long? id, string title)
        {
            using (var unitOfWork = this._unitOfWorkFactory.Connect())
            {
                var result = unitOfWork.Get<IRecipeRepository>().GetRecipesBy(id, title);
                return result;
            }
        }

        public List<Recipe> GetRecipes()
        {
            return this.GetRecipesBy(null, null);
        }

        public void AddOrUpdate(Recipe recipe)
        {
            using (var unitOfWork = this._unitOfWorkFactory.Connect())
            {
                unitOfWork.Get<IRecipeRepository>().AddOrUpdate(recipe);
            }
        }
    }
}
