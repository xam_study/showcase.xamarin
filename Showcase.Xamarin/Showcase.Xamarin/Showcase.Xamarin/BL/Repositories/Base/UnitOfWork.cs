﻿using System;
using Realms;
using Showcase.Xamarin.BL.Contracts.Repositories.Base;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Common;

namespace Showcase.Xamarin.BL.Repositories.Base
{
    public class UnitOfWork : IUnitOfWork, IInitializeWithIoc
    {
        public UnitOfWork(Realm realm)
        {
            this.Context = realm;
        }

        private IIocService _iocService;
        protected Realm Context { get; private set; }

        public TRepository Get<TRepository>() where TRepository : class, IRepository
        {
            var repository = this._iocService.Resolve<TRepository>();
            if (repository is BaseRepository<Realm>)
            {
                (repository as BaseRepository<Realm>).SetContext(this.Context);
                return repository;
            }
            else
            {
                throw new Exception("A repository should inherit BaseRepository<IDisposable> base class");
            }
        }

        public void Initialize(IIocService iocService)
        {
            this._iocService = iocService;
        }

        public void Dispose()
        {
            this.Context.Dispose();
        }
    }
}
