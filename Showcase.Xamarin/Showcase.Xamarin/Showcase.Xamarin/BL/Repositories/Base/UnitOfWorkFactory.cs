﻿using Realms;
using Showcase.Xamarin.BL.Contracts.Repositories.Base;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Common;

namespace Showcase.Xamarin.BL.Repositories.Base
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory, IInitializeWithIoc
    {
        private IIocService _iocService;

        public IUnitOfWork Connect()
        {
            return this._iocService.ResolveWithParamByType<IUnitOfWork, Realm>(Realm.GetInstance());
        }

        public void Initialize(IIocService iocService)
        {
            this._iocService = iocService;
        }
    }  
}
