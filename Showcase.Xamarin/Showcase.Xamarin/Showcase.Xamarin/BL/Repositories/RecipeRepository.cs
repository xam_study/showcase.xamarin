﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Realms;
using Showcase.Xamarin.BL.Contracts.Repositories;
using Showcase.Xamarin.BL.Contracts.Repositories.Base;
using Showcase.Xamarin.BL.Contracts.Repositories.DbEntities;
using Showcase.Xamarin.Extensions;
using Showcase.Xamarin.Models.Recipe;

namespace Showcase.Xamarin.BL.Repositories
{
    public class RecipeRepository : BaseRepository<Realm>, IRecipeRepository
    {
        public void RemoveAll()
        {
            this.Context.Write(() => this.Context.RemoveAll());
        }

        public List<Recipe> GetRecipes()
        {
            return this.GetRecipesByWorker(null).Select(this.ConvertDbRecipeToModelRecipe).ToList(); //.Select(this.ConvertDbRecipeToModelRecipe).ToList();
        }

        public List<Recipe> GetRecipesBy(long? id, string title)
        {
            var recipes = this.GetRecipesByWorker
            (c =>
                (c.Id == (id ?? c.Id)) &&
                (c.Title == (!string.IsNullOrWhiteSpace(title) ? title : c.Title))
            ).ToList();
            return recipes.Select(this.ConvertDbRecipeToModelRecipe).ToList();//.Select(this.ConvertDbRecipeToModelRecipe).ToList();
        }

        private List<DbRecipe> GetRecipesByWorker(Expression<Func<Recipe, bool>> expression)
        {
            var result = this.Context.All<DbRecipe>().ToList().Where
            (
                c => expression?.Compile(this.ConvertDbRecipeToModelRecipe(c)) ?? true
            );
            return result.ToList();
        }

        public void AddOrUpdate(Recipe recipe)
        {
            var foundRecipe = this.GetRecipesByWorker(c => c.Id == recipe.Id).FirstOrDefault();
            bool isAlreadyExist = foundRecipe != null;
            //todo:
            if (!isAlreadyExist)
                foundRecipe = this.ConvertModelRecipeToDbRecepe(recipe);

            this.Context.Write(() =>
            {
                if (isAlreadyExist)
                {
                    this.ConvertModelRecipeToDbRecepe(recipe, foundRecipe);
                }
                this.Context.Add(foundRecipe, isAlreadyExist);
            });
        }

        #region Converters
        private Recipe ConvertDbRecipeToModelRecipe(DbRecipe db)
        {
            return new Recipe()
            {
                Id = db.Id,
                Description = db.Description,
                ImageUrl = db.ImageUrl,
                Title = db.Title
            };
        }

        private DbRecipe ConvertModelRecipeToDbRecepe(Recipe modelRecipe)
        {
            return new DbRecipe()
            {
                Id = modelRecipe.Id,
                Description = modelRecipe.Description,
                ImageUrl = modelRecipe.ImageUrl,
                Title = modelRecipe.Title
            };
        }

        private DbRecipe ConvertModelRecipeToDbRecepe(Recipe modelRecipe, DbRecipe result)
        {
            result.Id = modelRecipe.Id;
            result.Description = modelRecipe.Description;
            result.ImageUrl = modelRecipe.ImageUrl;
            result.Title = modelRecipe.Title;
            return result;
        }

        #endregion
    }
}
