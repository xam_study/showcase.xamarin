﻿using System.Collections.Generic;
using Showcase.Xamarin.BL.Contracts.Repositories.Base;
using Showcase.Xamarin.Models.Recipe;

namespace Showcase.Xamarin.BL.Contracts.Repositories
{
    public interface IRecipeRepository : IRepository
    {
        void RemoveAll();
        List<Recipe> GetRecipes();
        List<Recipe> GetRecipesBy(long? id, string title);
        void AddOrUpdate(Recipe recipe);
    }
}
