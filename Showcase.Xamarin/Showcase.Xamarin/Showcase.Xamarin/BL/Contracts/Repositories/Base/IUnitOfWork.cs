﻿using System;

namespace Showcase.Xamarin.BL.Contracts.Repositories.Base
{
    public interface IUnitOfWork : IDisposable
    {
        TRepository Get<TRepository>() where TRepository : class, IRepository;
    }
}
