﻿namespace Showcase.Xamarin.BL.Contracts.Repositories.Base
{
    public interface IUnitOfWorkFactory 
    {
        IUnitOfWork Connect();
    }
}
