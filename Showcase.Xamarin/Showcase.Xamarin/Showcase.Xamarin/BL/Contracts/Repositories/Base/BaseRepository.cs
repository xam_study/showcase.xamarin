﻿using System;

namespace Showcase.Xamarin.BL.Contracts.Repositories.Base
{
    public abstract class BaseRepository<TContext> where TContext : class, IDisposable 
    {
        private TContext _context;
        protected TContext Context
        {
            get
            {
                if (_context == null)
                {
                    throw new Exception("A repository should be called by UnitOfWork factory");
                }

                return _context;
            }
        }

        public void SetContext(TContext context)
        {
            this._context = context;
        }
    }
}
