﻿using Realms;

namespace Showcase.Xamarin.BL.Contracts.Repositories.DbEntities
{
    public class DbRecipe : RealmObject
    {
        //[PrimaryKey] ---> Migration is required due to the following errors: - Primary Key for class 'DbRecipe' has been added. (See inner exception for details.
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
    }
}
