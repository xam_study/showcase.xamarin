﻿using System.Collections.Generic;
using Showcase.Xamarin.Models.Recipe;

namespace Showcase.Xamarin.BL.Contracts.Services
{
    public interface IRecipeService
    {
        List<Recipe> GetRecipes();
        void RemoveAll();
        List<Recipe> GetRecipesBy(long? id, string title);
        void AddOrUpdate(Recipe recipe);
    }
}
