﻿using System.Threading.Tasks;

namespace Showcase.Xamarin.BL.Contracts.Services
{
    public interface IResourcesService
    {
        Task<T> GetImageFromUrl<T>(string url) where T : class;
    }
}
