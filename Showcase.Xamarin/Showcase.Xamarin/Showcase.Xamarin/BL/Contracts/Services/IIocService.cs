﻿using System;
using System.Collections.Generic;

namespace Showcase.Xamarin.BL.Contracts.Services
{
    public interface IIocService
    {
        TType Resolve<TType>() where TType : class;
        TType ResolveWithParamByName<TType, TParamType>(string paramName, TParamType paramValue) where TType : class;
        TType ResolveWithParamByName<TType, TParamType>(IEnumerable<KeyValuePair<string, TParamType>> @params) where TType : class;
        TType ResolveWithParamByType<TType, TParamType>(TParamType paramValue) where TType : class;
        TType ResolveWithParamByType<TType, TParamType>(IEnumerable<TParamType> @params) where TType : class;
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class InjectAttribute : System.Attribute
    {
        /// <summary>
        /// Use this attribute as true only for first object initialization in launch
        /// </summary>
        public bool StartPoint { get; set; }
    }
}
