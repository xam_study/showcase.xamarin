﻿using System.Collections.Generic;

namespace Showcase.Xamarin.BL.Contracts.Services
{
    public interface IExternalContentService
    {
        KeyValuePair<string, string>[] GetRssContent(string url);
    }
}
