﻿using Showcase.Xamarin.ViewModels.Contracts.Recipe;
using Showcase.Xamarin.ViewModels.Recipe;

namespace Showcase.Xamarin.BL.Contracts.Controllers
{
    public interface IRecipeController
    {
        RecipeAlbumViewModel GetRecipeAlbumViewModel();
        void RemoveAll();
        void UpdateRecipe(IRecipeViewModel viewModel);
        void UpdateRecipe(string description, int id);
        void AddRecipe(IRecipeViewModel previousViewModel);
        string GenerateNewRandomDescription();
    }
}
