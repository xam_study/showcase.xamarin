﻿using System;
using System.Linq;
using Showcase.Xamarin.BL.Contracts.Controllers;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.Models.Recipe;
using Showcase.Xamarin.ViewModels.Contracts.Builders;
using Showcase.Xamarin.ViewModels.Contracts.Recipe;
using Showcase.Xamarin.ViewModels.Recipe;

namespace Showcase.Xamarin.BL.Controllers
{
    public class RecipeController : IRecipeController
    {
        private readonly IRecipeService _recipeService;
        private readonly IViewModelBuilder<RecipeAlbum, RecipeAlbumViewModel> _recipeViewModelBuilder;
        private readonly IExternalContentService _externalService;

        public RecipeController
        (
            IRecipeService recipeService, 
            IViewModelBuilder<RecipeAlbum, RecipeAlbumViewModel> recipeViewModelBuilder,
            IExternalContentService externalContentService)
        {
            this._recipeService = recipeService;
            this._recipeViewModelBuilder = recipeViewModelBuilder;
            this._externalService = externalContentService;
        }

        public RecipeAlbumViewModel GetRecipeAlbumViewModel()
        {
            var recipes = this._recipeService.GetRecipes().ToList();
            if (!recipes.Any())
            {
                this._recipeService.AddOrUpdate(new Recipe() { Id = 1, Title = "Test_from" + DateTime.Now, Description = "Description1", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 2, Title = "Test_from" + DateTime.Now, Description = "Description2", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 3, Title = "Test_from" + DateTime.Now, Description = "Description3", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 4, Title = "Test_from" + DateTime.Now, Description = "Description4", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 5, Title = "Test_from" + DateTime.Now, Description = "Description5", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 6, Title = "Test_from" + DateTime.Now, Description = "Description6", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 7, Title = "Test_from" + DateTime.Now, Description = "Description7", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 8, Title = "Test_from" + DateTime.Now, Description = "Description8", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 9, Title = "Test_from" + DateTime.Now, Description = "Description9", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 10, Title = "Test_from" + DateTime.Now, Description = "Description10", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 11, Title = "Test_from" + DateTime.Now, Description = "Description11", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 12, Title = "Test_from" + DateTime.Now, Description = "Description12", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 13, Title = "Test_from" + DateTime.Now, Description = "Description13", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 14, Title = "Test_from" + DateTime.Now, Description = "Description14", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 15, Title = "Test_from" + DateTime.Now, Description = "Description15", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 16, Title = "Test_from" + DateTime.Now, Description = "Description16", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 17, Title = "Test_from" + DateTime.Now, Description = "Description17", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 18, Title = "Test_from" + DateTime.Now, Description = "Description18", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 19, Title = "Test_from" + DateTime.Now, Description = "Description19", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 20, Title = "Test_from" + DateTime.Now, Description = "Description20", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 21, Title = "Test_from" + DateTime.Now, Description = "Description21", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 22, Title = "Test_from" + DateTime.Now, Description = "Description22", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 23, Title = "Test_from" + DateTime.Now, Description = "Description23", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 24, Title = "Test_from" + DateTime.Now, Description = "Description24", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 25, Title = "Test_from" + DateTime.Now, Description = "Description25", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 26, Title = "Test_from" + DateTime.Now, Description = "Description26", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 27, Title = "Test_from" + DateTime.Now, Description = "Description27", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 28, Title = "Test_from" + DateTime.Now, Description = "Description28", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 29, Title = "Test_from" + DateTime.Now, Description = "Description29", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 30, Title = "Test_from" + DateTime.Now, Description = "Description30", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 31, Title = "Test_from" + DateTime.Now, Description = "Description31", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 32, Title = "Test_from" + DateTime.Now, Description = "Description32", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 33, Title = "Test_from" + DateTime.Now, Description = "Description33", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 34, Title = "Test_from" + DateTime.Now, Description = "Description34", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 35, Title = "Test_from" + DateTime.Now, Description = "Description35", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 36, Title = "Test_from" + DateTime.Now, Description = "Description36", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 37, Title = "Test_from" + DateTime.Now, Description = "Description37", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 38, Title = "Test_from" + DateTime.Now, Description = "Description38", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 39, Title = "Test_from" + DateTime.Now, Description = "Description39", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 40, Title = "Test_from" + DateTime.Now, Description = "Description40", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 41, Title = "Test_from" + DateTime.Now, Description = "Description41", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 42, Title = "Test_from" + DateTime.Now, Description = "Description42", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 43, Title = "Test_from" + DateTime.Now, Description = "Description43", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                this._recipeService.AddOrUpdate(new Recipe() { Id = 44, Title = "Test_from" + DateTime.Now, Description = "Description44", ImageUrl = "http://remarkablecars.com/main/chevrolet/chevrolet-00028.jpg" });
                recipes = this._recipeService.GetRecipes().ToList();
            }

            var recipeAlbum = new RecipeAlbum(recipes);
            return this._recipeViewModelBuilder.BuildViewModel(recipeAlbum);
        }

        public void UpdateRecipe(IRecipeViewModel viewModel)
        {
            var recipe = this._recipeService.GetRecipesBy((viewModel as IId).Id, null).FirstOrDefault();
            if (recipe != null)
            {
                recipe.Title = viewModel.Title;
                this._recipeService.AddOrUpdate(recipe);
            }
        }

        public void UpdateRecipe(string description, int id)
        {
            var recipe = this._recipeService.GetRecipesBy(id, null).FirstOrDefault();
            if (recipe != null)
            {
                recipe.Description = description;
                this._recipeService.AddOrUpdate(recipe);
            }
        }

        public void AddRecipe(IRecipeViewModel previousLastViewModel)
        {
            this._recipeService.AddOrUpdate(new Recipe()
            {
                Description = previousLastViewModel.Description + "_new",
                Id = (previousLastViewModel as IId).Id + 10,
                Title = previousLastViewModel.Title + "_new",
                ImageUrl = previousLastViewModel.ImageUrl + "_new"
            });
        }

        public void RemoveAll()
        {
            this._recipeService.RemoveAll();
        }

        public string GenerateNewRandomDescription()
        {
            var rss = this._externalService.GetRssContent("http://www.bashorg.org/rss.xml");
            return rss != null && rss.Any() ? rss[new Random().Next(0, rss.Length - 1)].Value : "Rss content is not found";
        }
    }
}
