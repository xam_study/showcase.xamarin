﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using Microsoft.Practices.ServiceLocation;
using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.Extensions;

namespace Showcase.Xamarin
{
    public static class Bootstrapper
    {
        //todo: delete this collection
        private static readonly List<string> RegisteredAssemblyList = new List<string>();

        private static IContainer _container;
        /// <summary>
        /// This property should be used only before Build
        /// </summary>
        private static readonly ContainerBuilder ContainerBuilder = CreateContainerBuilder();

        private static ContainerBuilder CreateContainerBuilder()
        {
            var container = new ContainerBuilder();
            container.RegisterCallback(x =>
            {
                x.Registered += (sender, args) =>
                {
                    args.ComponentRegistration.Activating += (o, eventArgs) =>
                    {
                        (eventArgs.Instance as IInitializeWithIoc)?.Initialize(eventArgs.Context.Resolve<IIocService>());
                    };

                    args.ComponentRegistration.Activated += (o, eventArgs) =>
                    {
                        (eventArgs.Instance as IInitialize)?.Initialize();
                    };
                };
            });

            return container;
        }

        public static void RegisterAssemblyTypes(Assembly assembly)
        {
            if (!RegisteredAssemblyList.Contains(assembly.FullName))
            {
                RegisteredAssemblyList.Add(assembly.FullName);

                if (_container != null)
                {
                    throw new Exception("The assembly shoud be registered before Build");
                }
                ContainerBuilder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces();
            }
        }

        private static void InitializeServiceLocator(IComponentContext container)
        {
            var csl = new AutofacServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => csl);
        }

        public static TType Resolve<TType>() where TType : class
        {
            if (_container == null)
            {
                throw new Exception("The container should be initialized");
            }

            return _container.Resolve<TType>();
        }

        public static object Resolve(Type type)
        {
            if (_container == null)
            {
                throw new Exception("The container should be initialized");
            }

            return _container.Resolve(type);
        }

        public static object Run(Type type)
        {
            var currentAssemblyTypeInfo = typeof(Bootstrapper).GetTypeInfo();
            RegisterAssemblyTypes(currentAssemblyTypeInfo.Assembly);
            _container = ContainerBuilder.Build();

            InitializeServiceLocator(_container);

            return _container.SafeResolve(type);
        }

        public static TType Run<TType>() where TType : class
        {
            return (TType)Run(typeof(TType));
        }

        public static bool IsInitialized => _container != null;
    }
}
