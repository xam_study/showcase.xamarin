﻿using System;
using Showcase.Xamarin.ViewModels.Contracts;

namespace Showcase.Xamarin.Common
{
    public class EventArgsWithT<T> : EventArgs
    {
        public T Value { get; set; }

        public static EventArgsWithT<T> Create(T value)
        {
            return new EventArgsWithT<T>
            {
                Value = value
            };
        }
    }

    public class EventArgsWithT<TPrevious, TNew> : EventArgsWithT<TPrevious>
        where TPrevious : IViewModel
    {
        public TNew NewValue { get; set; }

        public static EventArgsWithT<TPrevious, TNew> Create(TPrevious previous, TNew @new)
        {
            return new EventArgsWithT<TPrevious, TNew>
            {
                Value = previous,
                NewValue = @new
            };
        }
    }
} 
