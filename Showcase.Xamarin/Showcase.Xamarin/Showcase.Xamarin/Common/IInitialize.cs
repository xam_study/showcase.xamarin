﻿using Showcase.Xamarin.BL.Contracts.Services;

namespace Showcase.Xamarin.Common
{
    public interface IInitialize
    {
        void Initialize();
    }

    public interface IInitializeWithIoc
    {
        void Initialize(IIocService iocService);
    }
}
