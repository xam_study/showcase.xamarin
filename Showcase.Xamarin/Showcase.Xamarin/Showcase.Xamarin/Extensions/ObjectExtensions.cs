﻿using System;
using System.Linq;
using System.Reflection;
using Showcase.Xamarin.BL.Contracts.Services;

namespace Showcase.Xamarin.Extensions
{
    public static class ObjectExtensions
    {
        public static void InitializeObjectByIocContainerAndAddingGlobalHandlers<TObject>
        (
            this TObject @object
        ) where TObject : class
        {
            InitializeObjectByIocContainerAndAddingGlobalHandlers(@object, null, null);
        }

        public static void InitializeObjectByIocContainerAndAddingGlobalHandlers<TObject>
        (
            this TObject @object, Action globalHandler, Func<Type, bool> additionalCheck
        ) where TObject : class
        {
            var type = @object.GetType();

            //set handlers
            {
                if (additionalCheck?.Invoke(type) ?? false)
                {
                    globalHandler?.Invoke();
                }
            }

            var typeInfo = type.GetTypeInfo();
            var baseTypeInfo = typeInfo.BaseType.GetTypeInfo();
            PropertyInfo[] properties = typeInfo.DeclaredProperties.Union(baseTypeInfo.DeclaredProperties).ToArray();
            FieldInfo[] fields = typeInfo.DeclaredFields.Union(baseTypeInfo.DeclaredFields).ToArray();

            // ReSharper disable once ConvertToLocalFunction
            Func<PropertyInfo, FieldInfo, Type> getType = (p, f) => p != null ? p.PropertyType : f?.FieldType;

            var propsAndAttributes = properties.Select(c => new
            {
                Prop = c,
                Attributes = c.GetCustomAttributes<InjectAttribute>(true),
                Field = (FieldInfo)null,
            })
            .Union(fields.Select(c => new
            {
                Prop = (PropertyInfo)null,
                Attributes = c.GetCustomAttributes<InjectAttribute>(true),
                Field = c
            }))
            .ToList();
            
            foreach (var property in propsAndAttributes.Where(p => p.Attributes.Any()))
            {
                object instance;
                if (/*property.Attributes.Any(c => c.StartPoint) &&*/ (additionalCheck?.Invoke(type) ?? true) && !Bootstrapper.IsInitialized)
                {
                    Bootstrapper.RegisterAssemblyTypes(type.GetTypeInfo().Assembly);
                    instance = Bootstrapper.Run(getType(property.Prop, property.Field));
                }
                else
                {
                    instance = Bootstrapper.Resolve(getType(property.Prop, property.Field));
                }
                if (instance == null)
                {
                    throw new InvalidOperationException("Could not resolve type " + getType(property.Prop, property.Field).ToString());
                }

                if (property.Prop != null)
                {
                    property.Prop.SetValue(@object, instance);
                }
                else
                {
                    property.Field.SetValue(@object, instance);
                }
            }
        }
    }
}
