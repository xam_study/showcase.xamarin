﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Showcase.Xamarin.Extensions
{
    public static class SerializeExtensions
    {
        public static string Serialize(this object val)
        {
            XmlSerializer xsSubmit = new XmlSerializer(val.GetType());
            string xml;

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, val);
                    xml = sww.ToString(); 
                }
            }
            return xml;
        }

        public static T Deserialize<T>(this string objectData)
        {
            return (T)Deserialize(objectData, typeof(T));
        }

        public static object Deserialize(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }
    }
}
