﻿using Showcase.Xamarin.BL.Contracts.Services;
using Showcase.Xamarin.ViewModels.Contracts;
using Showcase.Xamarin.Views;

namespace Showcase.Xamarin.Extensions
{
    public static class ViewExtensions
    {
        /// <summary>
        /// Try to resolve new type with external param. Then, the value will be check on ViewContext creating if type is the View
        /// </summary>
        /// <typeparam name="TType">Usualy it's view interface</typeparam>
        /// <typeparam name="TParamType">It's external type param, which is a native pbject usually</typeparam>
        /// <typeparam name="TViewModel">The ViewModel which is connected with View. In case if you want to subscribe on events, the ViewModel should contain events</typeparam>
        /// <param name="service">Ioc service</param>
        /// <param name="paramValue">The external value</param>
        /// <returns>Return TType</returns>
        public static TType ResolveAndCheckInnerParams<TType, TParamType, TViewModel>(this IIocService service, TParamType paramValue)
            where TType : class
            where TViewModel : class, IViewModel 
        {
            var value = service.ResolveWithParamByType<TType, TParamType>(paramValue);

            if (value is IView<TViewModel> view)
            {
                ViewContext<TViewModel>.CheckViewContext(view);
            }

            return value;
        }
    }
}
