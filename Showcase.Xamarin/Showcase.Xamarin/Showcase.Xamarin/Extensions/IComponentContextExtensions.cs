﻿using Autofac;
using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Showcase.Xamarin.Extensions
{
    public static class ComponentContextExtensions
    {
        public static TType SafeResolve<TType>(this IComponentContext context) where TType : class
        {
            return context.IsRegistered<TType>() ? context.Resolve<TType>() : context.ResolveUnregistered<TType>();
        }

        public static object SafeResolve(this IComponentContext context, Type type)
        {
            return context.IsRegistered(type) ? context.Resolve(type) : context.ResolveUnregistered(type);
        }

        public static TType ResolveUnregistered<TType>(this IComponentContext componentContext) where TType : class
        {
            return (ResolveUnregistered(componentContext, typeof(TType)) ?? default(TType)) as TType;
        }

        public static object ResolveUnregistered(this IComponentContext componentContext, Type type)
        {
            var scope = componentContext.Resolve<ILifetimeScope>();
            using (var innerScope = scope.BeginLifetimeScope(b => b.RegisterType(type)))
            {
                if (innerScope.ComponentRegistry.TryGetRegistration(new TypedService(type), out var reg))
                {
                    var parameters = new List<Parameter>();
                    var constructors = type.GetTypeInfo().DeclaredConstructors;
                    foreach (var constructor in constructors)
                    {
                        var @params = constructor.GetParameters();
                        foreach (var param in @params)
                        {
                            var paramForRegister = new ResolvedParameter
                            (
                                (info, context) => context.IsRegistered(info.GetType()),
                                (info, context) => context.SafeResolve(info.GetType())
                            );
                            parameters.Add(paramForRegister);
                        }
                    }
                    return componentContext.ResolveComponent(reg, parameters);
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
