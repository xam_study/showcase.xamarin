﻿using System;
using System.Linq.Expressions;

namespace Showcase.Xamarin.Extensions
{
    public static class ExpressionExtensions
    {
        public static TResult Compile<TParam, TResult>(this Expression<Func<TParam, TResult>> expression, TParam param)
        {
            var compiled = expression.Compile();
            return compiled(param);
        }
    }
}
