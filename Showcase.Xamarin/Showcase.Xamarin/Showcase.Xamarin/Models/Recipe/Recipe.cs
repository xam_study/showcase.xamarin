﻿using Showcase.Xamarin.Models.Contracts;

namespace Showcase.Xamarin.Models.Recipe
{
    public class Recipe : IModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
    }
}
