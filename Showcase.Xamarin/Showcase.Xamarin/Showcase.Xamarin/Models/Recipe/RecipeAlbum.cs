﻿using System.Collections.Generic;
using Showcase.Xamarin.Models.Contracts;

namespace Showcase.Xamarin.Models.Recipe
{
    public class RecipeAlbum : IModel
    {
        public RecipeAlbum()
        {
            if (this.Recipes == null) this.Recipes = new List<Models.Recipe.Recipe>();
        }

        public RecipeAlbum(List<Models.Recipe.Recipe> recipes)
        {
            this.Recipes = recipes;
        }

        public List<Models.Recipe.Recipe> Recipes { get; set; }

        public string Name { get; set; }

        public int Count => this.Recipes.Count;
        public int Id { get; set; }
    }
}
