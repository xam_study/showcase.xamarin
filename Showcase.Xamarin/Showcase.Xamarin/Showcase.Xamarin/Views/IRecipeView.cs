﻿using Showcase.Xamarin.Common;
using Showcase.Xamarin.ViewModels.Recipe;

namespace Showcase.Xamarin.Views
{
    public interface IRecipeView : IInitialize, IView<RecipeViewModel>
    {
    }
}