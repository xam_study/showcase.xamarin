﻿using Showcase.Xamarin.ViewModels.Recipe;

namespace Showcase.Xamarin.Views
{
    public interface IRecipeDetailsView : IView<RecipeDetailsViewModel>
    {
    }
}
