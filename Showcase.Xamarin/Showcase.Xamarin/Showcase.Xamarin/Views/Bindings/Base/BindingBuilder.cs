﻿using System;
using Showcase.Xamarin.ViewModels.Contracts;
using Showcase.Xamarin.Views.Bindings.Contracts;

namespace Showcase.Xamarin.Views.Bindings.Base
{
    public class BindingBuilder<TViewModel, TView> : IBindingBuilder<TViewModel, TView> where TView : IView<TViewModel>
        where TViewModel : class, IViewModel
    {
        //todo: refactor
        protected Action<TViewModel> InitializeItemViewModelAction;

        public void Bind(TViewModel viewModel, TView view) 
        {
            if (view is IView<TViewModel> viewWithViewModel)
            {
                if (viewWithViewModel.ViewContext is ViewContext<TViewModel> viewContext)
                {
                    viewContext.SetViewModel(viewModel);
                }
                else
                {
                    throw new ArgumentException("The ViewContext is not initialized correctly");
                }
            }
            else
            {
                throw new ArgumentException("The view should have ViewModel link");
            }

            view.Refresh();

            //Android: Set init status for viewmodel on each bind
            if (!viewModel.ViewModelContext.IsEventsSubscribed)
            {
                this.InitializeItemViewModelAction(viewModel);
                ViewModelContext.SetEventSubscribeStatus(viewModel.ViewModelContext, view.Refresh);
            }

            //Android: Set init status for view for an each view creating
            if (!viewWithViewModel.ViewContext.IsInitialized)
            {
                ViewContext<TViewModel>.SetEventSunscriptionStatus(view, true);
            }
        }

        public void EventSubscription(Action<TViewModel> subscribeAction)
        {
            this.InitializeItemViewModelAction = subscribeAction;
        }
    }
}
