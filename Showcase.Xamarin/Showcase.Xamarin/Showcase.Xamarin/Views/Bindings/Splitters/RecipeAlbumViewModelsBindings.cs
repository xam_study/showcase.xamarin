﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Showcase.Xamarin.ViewModels.Recipe;
using Showcase.Xamarin.Views.Bindings.Splitters.Base;

namespace Showcase.Xamarin.Views.Bindings.Splitters
{
    //todo: think about deleting of this class
    public class RecipeAlbumViewModelsBindings : SplitViewModelsAndInnerItemViewModelBindings<RecipeAlbumViewModel, RecipeViewModel, IRecipeView>
    {
        public override Expression<Func<RecipeAlbumViewModel, IEnumerable<RecipeViewModel>>> CollectionProperty => (source) => source.Recipes;
    }
}
