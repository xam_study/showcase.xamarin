﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.ViewModels.Contracts;
using Showcase.Xamarin.Views.Bindings.Base;
using Showcase.Xamarin.Views.Bindings.Contracts;

namespace Showcase.Xamarin.Views.Bindings.Splitters.Base
{
    public abstract class SplitViewModelsAndInnerItemViewModelBindings<TSourceCollectionViewModel, TItemViewModel, TView> : 
        IListViewModelsBindingBuilder<TSourceCollectionViewModel, TItemViewModel, TView>, IInitialize
        where TSourceCollectionViewModel : class, IViewModel
        where TItemViewModel : class, IViewModel
        where TView : IView<TItemViewModel>
    {
        public TSourceCollectionViewModel ViewModel { get; private set; }
        protected Action<TItemViewModel> InitializeItemViewModelAction;
        protected IBindingBuilder<TItemViewModel, TView> Builder;
        public List<TItemViewModel> Collection => this.CollectionProperty.Compile()(this.ViewModel).ToList();
        public int Count => Collection?.Count ?? 0;

        public void Bind(TSourceCollectionViewModel sourceViewModel)
        {
            this.ViewModel = sourceViewModel;
        }

        public abstract Expression<Func<TSourceCollectionViewModel, IEnumerable<TItemViewModel>>> CollectionProperty { get; }

        public void Bind(int index, TView view)
        {
            var collectionCount = this.Collection.Count;
            if (index >= collectionCount)
            {
                throw new IndexOutOfRangeException("The index is out of collection");
            }

            var isEventsSubscribed = this.Collection[index].ViewModelContext.IsEventsSubscribed;
            if (!isEventsSubscribed)
            {
                this.Builder.EventSubscription(this.InitializeItemViewModelAction);
            }
            this.Builder.Bind(this.Collection[index], view);
        }

        public void EventSubscription(Action<TItemViewModel> initializeViewModelAction)
        {
            this.InitializeItemViewModelAction = initializeViewModelAction;
        }

        public void Initialize()
        {
            this.Builder = new BindingBuilder<TItemViewModel, TView>();
        }
    }
}
