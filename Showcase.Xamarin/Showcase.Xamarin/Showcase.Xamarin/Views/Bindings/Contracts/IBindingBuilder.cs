﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Showcase.Xamarin.ViewModels.Contracts;

namespace Showcase.Xamarin.Views.Bindings.Contracts
{
    public interface IBindingBuilder<TViewModel, in TView> 
        where TViewModel : class, IViewModel
        where TView : IView<TViewModel>
    {
        void Bind(TViewModel viewModel, TView view);
        void EventSubscription(Action<TViewModel> subscribeAction);
    }

    public interface IListViewModelsBindingBuilder<TViewModelsSource, TItemViewModel, in TView> /*: IRefreshState*/
        where TViewModelsSource : IViewModel
        where TItemViewModel : IViewModel
        where TView : IView<TItemViewModel>
    {
        void Bind(int index, TView view);
        List<TItemViewModel> Collection { get; }
        int Count { get; }
        TViewModelsSource ViewModel { get; }
        Expression<Func<TViewModelsSource, IEnumerable<TItemViewModel>>> CollectionProperty { get; }
        void Bind(TViewModelsSource sourceViewModel);
        void EventSubscription(Action<TItemViewModel> subscribeAction);
    }
}
