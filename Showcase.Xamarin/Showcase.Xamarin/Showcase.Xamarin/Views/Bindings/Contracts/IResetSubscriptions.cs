﻿namespace Showcase.Xamarin.Views.Bindings.Contracts
{
    public interface IResetSubscriptions
    {
        void Reset();
    }
}
