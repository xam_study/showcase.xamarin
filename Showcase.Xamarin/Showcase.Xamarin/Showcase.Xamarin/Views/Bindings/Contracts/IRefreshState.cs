﻿namespace Showcase.Xamarin.Views.Bindings.Contracts
{
    public interface IRefreshState
    {
        void Refresh();
    }
}
