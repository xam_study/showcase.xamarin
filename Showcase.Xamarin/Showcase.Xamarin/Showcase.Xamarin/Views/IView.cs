﻿using System;
using Showcase.Xamarin.ViewModels.Contracts;
using Showcase.Xamarin.Views.Bindings.Contracts;

namespace Showcase.Xamarin.Views
{
    public interface IView<out TViewModel> : IRefreshState  where TViewModel : IViewModel
    {
        IViewContext<TViewModel> ViewContext { get; }
    }

    public interface IViewContext<out TViewModel> where TViewModel : IViewModel
    {
        bool IsInitialized { get; }
        TViewModel ViewModel { get; }
    }

    public class ViewContext<TViewModel> : IViewContext<TViewModel> where TViewModel : class, IViewModel
    {
        public bool IsInitialized { get; private set; }
        public TViewModel ViewModel { get; private set; }

        public void SetViewSubscriptionStatus(bool value)
        {
            this.IsInitialized = value;
        }

        public void SetViewModel(TViewModel viewModel)
        {
            this.ViewModel = new WeakReference(viewModel).Target as TViewModel;
        }

        public static void CheckViewContext(IView<TViewModel> view)
        {
            if (view.ViewContext == null)
            {
                //todo: think about filling ViewContext here
                throw new ArgumentException("The ViewContext should be initialized in View constructor");
            }
        }

        public static IViewContext<TViewModel> CreateViewContext(IView<TViewModel> view)
        {
            return view.ViewContext ?? new ViewContext<TViewModel>();
        }

        public static void SetEventSunscriptionStatus(IView<TViewModel> view, bool value)
        {
            (view.ViewContext as ViewContext<TViewModel>)?.SetViewSubscriptionStatus(value);
        }
    }    
}
