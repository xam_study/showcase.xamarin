﻿using System;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.ViewModels.Contracts.Recipe;

namespace Showcase.Xamarin.ViewModels.Recipe
{
    public class RecipeDetailsViewModel : NotifyPropertyViewModel, IRecipeDetailsViewModel
    {
        public string Description { get; set; }
        public /*event*/ EventHandler<EventArgsWithT<IRecipeDetailsViewModel>> OnGenerateNewDescription;
        public int Id { get; set; }
    }
}
