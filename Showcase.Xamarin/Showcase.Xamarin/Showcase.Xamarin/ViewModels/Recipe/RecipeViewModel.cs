﻿using System;
using Showcase.Xamarin.Common;
using Showcase.Xamarin.ViewModels.Contracts.Recipe;

namespace Showcase.Xamarin.ViewModels.Recipe
{
    public class RecipeViewModel : NotifyPropertyViewModel, IRecipeViewModel, IId
    {
        public int Id { get; set; }

        private string _imageUrl;
        public string ImageUrl
        {
            get => this._imageUrl;
            set
            {
                this._imageUrl = value;
                this.OnPropertyChanged(nameof(ImageUrl));
            }
        }

        private string _title;
        public string Title
        {
            get => this._title;
            set
            {
                this._title = value;
                this.OnPropertyChanged(nameof(Title));
            }
        }

        private string _description;

        public string Description
        {
            get => this._description;
            set
            {
                this._description = value;
                this.OnPropertyChanged(nameof(Description));
            }
        }

        //todo: to replace on event
        public /*event*/ EventHandler<EventArgsWithT<IRecipeViewModel, IRecipeViewModel>> OnDetailsClick;
    }
}
