﻿using Showcase.Xamarin.ViewModels.Contracts.Recipe;
using System.Collections.Generic;
using System.Linq;

namespace Showcase.Xamarin.ViewModels.Recipe
{
    public class RecipeAlbumViewModel : NotifyPropertyViewModel, IRecipeAlbumViewModel
    {
        //ObservableCollection for event notification
        public IEnumerable<RecipeViewModel> Recipes { get; set; }

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                this.OnPropertyChanged(nameof(Name));
            }
        }

        public int Count => this.Recipes.Count();
    }
}
