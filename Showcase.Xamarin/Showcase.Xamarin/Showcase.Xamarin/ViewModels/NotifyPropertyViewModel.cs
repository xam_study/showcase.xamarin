﻿using Showcase.Xamarin.Common;
using Showcase.Xamarin.ViewModels.Contracts;
using System;

namespace Showcase.Xamarin.ViewModels
{
    public abstract class NotifyPropertyViewModel : NotifyProperty, IViewModel
    {
        protected NotifyPropertyViewModel()
        {
            this.ViewModelContext = new ViewModelContext();
        }

        public IViewModelContext ViewModelContext { get; private set; }
    }
}
