﻿using System;

namespace Showcase.Xamarin.ViewModels.Contracts
{
    public interface IViewModel 
    {
        IViewModelContext ViewModelContext { get; }
    }

    public interface IViewModelContext
    {
        bool IsEventsSubscribed { get; }
        void RefreshView();
    }

    public class ViewModelContext : IViewModelContext
    {
        public bool IsEventsSubscribed { get; set; }
        public event EventHandler OnRefresh;
        public void RefreshView()
        {
            this.OnRefresh?.Invoke(this, EventArgs.Empty);
        }

        public static void SetEventSubscribeStatus(IViewModelContext viewModelContext, Action resreshViewAction)
        {
            if (viewModelContext is ViewModelContext castedViewModelContext)
            {
                castedViewModelContext.IsEventsSubscribed = true;
                castedViewModelContext.OnRefresh += (sender, args) =>
                {
                    resreshViewAction();
                };
            }
        }
    }
}
