﻿namespace Showcase.Xamarin.ViewModels.Contracts.Builders
{
    public interface IViewModelBuilder<in TModel, out TViewModel>
    {
        TViewModel BuildViewModel(TModel model);
    }
}
