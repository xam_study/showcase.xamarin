﻿using Showcase.Xamarin.Common;

namespace Showcase.Xamarin.ViewModels.Contracts.Recipe
{
    public interface IRecipeDetailsViewModel : IViewModel, IId
    {
        string Description { get; set; }
    }
}
