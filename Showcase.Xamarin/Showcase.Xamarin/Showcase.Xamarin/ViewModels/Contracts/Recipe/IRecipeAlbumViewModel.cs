﻿using System.Collections.Generic;
using Showcase.Xamarin.ViewModels.Recipe;

namespace Showcase.Xamarin.ViewModels.Contracts.Recipe
{
    public interface IRecipeAlbumViewModel : IViewModel
    {
        IEnumerable<RecipeViewModel> Recipes { get; set; }

        string Name { get; set; }

        int Count { get; }
    }
}
