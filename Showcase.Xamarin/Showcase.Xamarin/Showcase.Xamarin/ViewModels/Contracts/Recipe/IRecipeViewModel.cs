﻿namespace Showcase.Xamarin.ViewModels.Contracts.Recipe
{
    public interface IRecipeViewModel : IViewModel
    {
        string ImageUrl { get; set; }
        string Title { get; set; }
        string Description { get; set; }
    }
}
