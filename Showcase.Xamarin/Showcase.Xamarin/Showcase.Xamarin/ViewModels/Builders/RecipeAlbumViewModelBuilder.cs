﻿using System.Linq;
using Showcase.Xamarin.Models.Recipe;
using Showcase.Xamarin.ViewModels.Contracts.Builders;
using Showcase.Xamarin.ViewModels.Recipe;

namespace Showcase.Xamarin.ViewModels.Builders
{
    public class RecipeAlbumViewModelBuilder : IViewModelBuilder<RecipeAlbum, RecipeAlbumViewModel>
    {
        private readonly IViewModelBuilder<Models.Recipe.Recipe, RecipeViewModel> _itemViewModelBuilder;
        public RecipeAlbumViewModelBuilder(IViewModelBuilder<Models.Recipe.Recipe, RecipeViewModel> itemViewModelBuilder)
        {
            this._itemViewModelBuilder = itemViewModelBuilder;
        }

        public RecipeAlbumViewModel BuildViewModel(RecipeAlbum model)
        {
            return new RecipeAlbumViewModel()
            {
                Recipes = model.Recipes.Select(c => this._itemViewModelBuilder.BuildViewModel(c)).ToList(),
                Name = model.Name
            };
        }
    }
}
