﻿using Showcase.Xamarin.ViewModels.Contracts.Builders;
using Showcase.Xamarin.ViewModels.Recipe;

namespace Showcase.Xamarin.ViewModels.Builders
{
    public class RecipeMainViewModelBuilder : IViewModelBuilder<Models.Recipe.Recipe, RecipeViewModel>
    {
        public RecipeViewModel BuildViewModel(Models.Recipe.Recipe model)
        {
            return new RecipeViewModel
            {
                Id = model.Id,
                Description = model.Description,
                Title = model.Title,
                ImageUrl = model.ImageUrl
            };
        }
    }
}
