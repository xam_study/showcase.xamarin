﻿using Showcase.Xamarin.Extensions;
using UIKit;

namespace Showcase.Xamarin.iOS.UI.Views.Common
{
    public class InitializedUICollectionViewDelegateFlowLayout : UICollectionViewDelegateFlowLayout
    {
        public InitializedUICollectionViewDelegateFlowLayout()
        {
            this.InitializeObjectByIocContainerAndAddingGlobalHandlers();
        }
    }
}