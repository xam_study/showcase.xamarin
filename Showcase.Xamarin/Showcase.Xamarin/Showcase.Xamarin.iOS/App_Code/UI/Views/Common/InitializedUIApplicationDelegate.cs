﻿using Showcase.Xamarin.Extensions;
using UIKit;

namespace Showcase.Xamarin.iOS.UI.Views.Common
{
    public class InitializedUIApplicationDelegate : UICollectionViewDelegateFlowLayout
    {
        public InitializedUIApplicationDelegate() : base()
        {
            this.InitializeObjectByIocContainerAndAddingGlobalHandlers();
        }
    }
}
