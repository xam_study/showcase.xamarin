﻿using Showcase.Xamarin.Extensions;
using UIKit;

namespace Showcase.Xamarin.iOS.UI.Views.Common
{
    public class InitializedUICollectionViewController : UICollectionViewController
    {
        public InitializedUICollectionViewController()
        {
            this.InitializeObjectByIocContainer();
        }
    }
}
